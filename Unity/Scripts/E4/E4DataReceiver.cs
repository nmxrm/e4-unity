﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;

public class E4DataReceiver : MonoBehaviour
{
    public static E4DataReceiver Instance;

    public bool UseGameTime = true;

    public class AccelerationEvent : UnityEvent<int, int, int, double> { }
    public AccelerationEvent OnAccelerationReceived = new AccelerationEvent();

    public class E4DataEvent : UnityEvent<float, double> { }

    public E4DataEvent OnSkinTemperatureReceived = new E4DataEvent();
    public E4DataEvent OnInterbeatIntervalHeartbeatReceived = new E4DataEvent();
    public E4DataEvent OnGalvanicSkinResponseReceived = new E4DataEvent();
    public E4DataEvent OnBloodVolumePulseReceived = new E4DataEvent();
    public E4DataEvent OnBatteryLevelReceived = new E4DataEvent();

    public class StatusChangedEvent : UnityEvent<string> { }
    public StatusChangedEvent OnStatusChanged = new StatusChangedEvent();

    public class WristStatusEvent : UnityEvent<bool> { }
    public WristStatusEvent OnWristStatusChanged = new WristStatusEvent();

    private UdpClient client;

    [SerializeField]
    private bool debug;

    [SerializeField]
    private string multicastIp = "239.255.42.99";

    [SerializeField]
    private int port = 1511;

    private Coroutine e4ReceivingRoutine;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    void Start()
    {
        client = new UdpClient();

        if (debug)
        {
            OnAccelerationReceived.AddListener((x, y, z, timestamp) => Debug.LogFormat("Acceleration: x:{0}, y:{1}, z:{2}, {3}", x, y, z, timestamp));
            OnSkinTemperatureReceived.AddListener((temp, timestamp) => Debug.LogFormat("Temp: {0}, {1}", temp, timestamp));
            OnBatteryLevelReceived.AddListener((bat, timestamp) => Debug.LogFormat("Battery: {0}, {1}", bat, timestamp));
            OnBloodVolumePulseReceived.AddListener((bvp, timestamp) => Debug.LogFormat("Bvp: {0}, {1}", bvp, timestamp));
            OnGalvanicSkinResponseReceived.AddListener((gsr, timestamp) => Debug.LogFormat("Gsr: {0}, {1}", gsr, timestamp));
            OnInterbeatIntervalHeartbeatReceived.AddListener((ibi, timestamp) => Debug.LogWarningFormat("Ibi: {0}, {1}", ibi, timestamp));
        }
    }
    
// *** DEBUG
    private float fakeHB = 60f;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            fakeHB += 5f;
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            fakeHB -= 5f;
        }
    }

    [ContextMenu("Fake HB routine")]
    public void DEBUGFakeHeartbeat()
    {
        StartCoroutine(DEBUGFakeHeartbeatRoutine());
    }

    IEnumerator DEBUGFakeHeartbeatRoutine()
    {
        while (true)
        {
            OnInterbeatIntervalHeartbeatReceived.Invoke(fakeHB, 0);

            yield return new WaitForSeconds(2f);
        }
    }
// *** DEBUG

    [ContextMenu("Start")]
    public void StartReceivingRecords()
    {
        IPAddress ip = IPAddress.Parse(multicastIp);
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, port);
        //IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);

        client.Client.ExclusiveAddressUse = false;

        //client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

        // Otherwise the game is blocked until the client receives a message.
        // Without blocking it throws an exception when not receiving a message.
        client.Client.Blocking = false;
        client.Client.Bind(RemoteIpEndPoint);

        client.Client.EnableBroadcast = true;

        client.JoinMulticastGroup(ip);

        e4ReceivingRoutine = StartCoroutine(ReceiveBytes(RemoteIpEndPoint));
        //ReceiveWithThread(RemoteIpEndPoint);
        //client.BeginReceive(ReceiveAsync, null);

        Debug.Log("Starting udp client!");
    }

    [ContextMenu("Stop")]
    public void StopReceivingRecords()
    {
        if (e4ReceivingRoutine != null)
        {
            StopCoroutine(e4ReceivingRoutine);

            e4ReceivingRoutine = null;
        }

        client.Client.Close();
        client.Close();
        ((IDisposable)client).Dispose();

        client = new UdpClient();

        Debug.Log("Stopping udp client!");
    }

    void OnDestroy()
    {
        StopReceivingRecords();

        if (t != null)
        {
            t.Abort();
        }
    }

    IEnumerator ReceiveBytes(IPEndPoint endPoint)
    {
        bool received = false;

        while (true)
        {
            try
            {
                //using (client = new UdpClient())
                //{


                var bytes = client.Receive(ref endPoint);

                //if (bytes != null)
                //{
                //    received = true;
                //}

                var stream = new MemoryStream(bytes);

                string msg = Encoding.ASCII.GetString(stream.ToArray());

                if (debug)
                {
                    Debug.LogFormat("Received from {0}:{1}: {2}", multicastIp, port, msg);
                }

                HandleMessage(msg);
                //}
            }
            catch (SocketException e)
            {
                //Debug.LogError(e.Message);

            }

            //if (received)
            //{
            //    yield return new WaitForSeconds(0.25f);

            //}
            //else
            //{
            yield return null;
            //}
        }
    }

    private void HandleMessage(string msg)
    {
        string[] splitMsgs = msg.Split('#');
        for (int i = 0; i < splitMsgs.Length; i++)
        {
            var splitValues = splitMsgs[i].Split(':');

            if (splitValues[0].Contains("acc"))
            {
                var splitData = splitValues[1].Split(';');

                int x = int.Parse(splitData[0]);
                int y = int.Parse(splitData[1]);
                int z = int.Parse(splitData[2]);

                double timestamp = double.Parse(splitData[3]);

                if (UseGameTime)
                {
                    timestamp = Time.time;
                }

                OnAccelerationReceived.Invoke(x, y, z, timestamp);
            }
            else if (splitValues[0].Contains("temp"))
            {
                var splitData = splitValues[1].Split(';');

                float temp = float.Parse(splitData[0]);
                double timestamp = double.Parse(splitData[1]);

                if (UseGameTime)
                {
                    timestamp = Time.time;
                }

                OnSkinTemperatureReceived.Invoke(temp, timestamp);
            }
            else if (splitValues[0].Contains("ibi"))
            {
                var splitData = splitValues[1].Split(';');

                float ibi = float.Parse(splitData[0]);
                double timestamp = double.Parse(splitData[1]);

                if (UseGameTime)
                {
                    timestamp = Time.time;
                }

                OnInterbeatIntervalHeartbeatReceived.Invoke(ibi, timestamp);
            }
            else if (splitValues[0].Contains("gsr"))
            {
                var splitData = splitValues[1].Split(';');

                float gsr = float.Parse(splitData[0]);
                double timestamp = double.Parse(splitData[1]);

                if (UseGameTime)
                {
                    timestamp = Time.time;
                }

                OnGalvanicSkinResponseReceived.Invoke(gsr, timestamp);
            }
            else if (splitValues[0].Contains("bat"))
            {
                var splitData = splitValues[1].Split(';');

                float bat = float.Parse(splitData[0]);
                double timestamp = double.Parse(splitData[1]);

                if (UseGameTime)
                {
                    timestamp = Time.time;
                }

                OnBatteryLevelReceived.Invoke(bat, timestamp);
            }
            else if (splitValues[0].Contains("bvp"))
            {
                var splitData = splitValues[1].Split(';');

                float bvp = float.Parse(splitData[0]);
                double timestamp = double.Parse(splitData[1]);

                if (UseGameTime)
                {
                    timestamp = Time.time;
                }

                OnBloodVolumePulseReceived.Invoke(bvp, timestamp);
            }
            else if (splitValues[0].Contains("status"))
            {
                string status = splitValues[1];

                OnStatusChanged.Invoke(status);
            }
            else if (splitValues[0].Contains("wrist"))
            {
                int value = int.Parse(splitValues[1]);

                bool onWrist;

                if (value == 0)
                {
                    onWrist = false;
                }
                else
                {
                    onWrist = true;
                }

                OnWristStatusChanged.Invoke(onWrist);
            }
        }

    }

    private void ReceiveAsync(IAsyncResult res)
    {
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, port);
        byte[] received = client.EndReceive(res, ref RemoteIpEndPoint);

        //Process codes
        Debug.Log(received);

        client.BeginReceive(new AsyncCallback(ReceiveAsync), null);
    }

    private Thread t;

    public void ReceiveWithThread(IPEndPoint endPoint)
    {
        t = new Thread(() =>
       {
           while (true)
           {
               try
               {
                   var bytes = client.Receive(ref endPoint);

                   var stream = new MemoryStream(bytes);

                   string msg = Encoding.ASCII.GetString(stream.ToArray());

                   if (debug)
                   {
                       Debug.LogFormat("Received from {0}:{1}: {2}", multicastIp, port, msg);
                   }

                   HandleMessage(msg);
               }
               catch (SocketException e)
               {
                   Debug.LogError(e.Message);

               }
           }
       });

        t.Start();
    }

    public void StartReceivingWithThread()
    {
        IPAddress ip = IPAddress.Parse(multicastIp);
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, port);
        //IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);

        client.Client.ExclusiveAddressUse = false;

        client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);

        // Otherwise the game is blocked until the client receives a message.
        // Without blocking it throws an exception when not receiving a message.
        client.Client.Blocking = false;
        client.Client.Bind(RemoteIpEndPoint);

        client.Client.EnableBroadcast = true;

        client.JoinMulticastGroup(ip);

        //e4ReceivingRoutine = StartCoroutine(ReceiveBytes(RemoteIpEndPoint));
        //ReceiveWithThread(RemoteIpEndPoint);
        //client.BeginReceive(ReceiveAsync, null);

        Debug.Log("Starting udp client!");

        t = new Thread(() =>
        {
            while (true)
            {
                try
                {
                    var bytes = client.Receive(ref RemoteIpEndPoint);

                    var stream = new MemoryStream(bytes);

                    string msg = Encoding.ASCII.GetString(stream.ToArray());

                    if (debug)
                    {
                        Debug.LogFormat("Received from {0}:{1}: {2}", multicastIp, port, msg);
                    }

                    HandleMessage(msg);
                }
                catch (SocketException e)
                {
                    //Debug.LogError(e.Message);

                }
            }
        });

        t.Start();

    }
}
