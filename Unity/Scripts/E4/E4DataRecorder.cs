﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProtoBuf;
using UnityEngine;

public class E4DataRecorder : MonoBehaviour
{
    public static E4DataRecorder Instance;

    private List<E4Data> recordedData;

    private Coroutine replayCoroutine;

    public bool StartRecordingOnStart = false;

    public string path = @"D:\";

    public bool UseGenericName = true;

    public bool SaveIbiCsv = true;

    public string filename = "test.e4d";

    public float replaySleepTime = 0.5f;

    public void Start()
    {
        //recordedData = new List<E4Data>();

        //AddAcceleration(5, 2, 6, 3.5);
        //AddAcceleration(8, 65, 7, 3.5324);
        //AddBvp(4.765f, 234.435);

        //StopRecording();

        if (StartRecordingOnStart)
        {
            StartRecording();
        }
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    [ContextMenu("Start recording")]
    public void StartRecording()
    {
        recordedData = new List<E4Data>();

        var dataReceiver = E4DataReceiver.Instance;

        dataReceiver.OnAccelerationReceived.AddListener(AddAcceleration);
        dataReceiver.OnSkinTemperatureReceived.AddListener(AddTemperature);
        dataReceiver.OnInterbeatIntervalHeartbeatReceived.AddListener(AddIbi);
        dataReceiver.OnGalvanicSkinResponseReceived.AddListener(AddGsr);
        dataReceiver.OnBloodVolumePulseReceived.AddListener(AddBvp);
        dataReceiver.OnBatteryLevelReceived.AddListener(AddBatteryLevel);
    }

    [ContextMenu("Stop recording")]
    public void StopRecording()
    {
        var dataReceiver = E4DataReceiver.Instance;

        dataReceiver.OnAccelerationReceived.RemoveListener(AddAcceleration);
        dataReceiver.OnSkinTemperatureReceived.RemoveListener(AddTemperature);
        dataReceiver.OnInterbeatIntervalHeartbeatReceived.RemoveListener(AddIbi);
        dataReceiver.OnGalvanicSkinResponseReceived.RemoveListener(AddGsr);
        dataReceiver.OnBloodVolumePulseReceived.RemoveListener(AddBvp);
        dataReceiver.OnBatteryLevelReceived.RemoveListener(AddBatteryLevel);

        if (SaveIbiCsv)
        {
            SaveIbiAsCsv();
        }

        if (recordedData != null && recordedData.Count > 0)
        {
            var data = recordedData.ToArray();

            if (UseGenericName)
            {
                filename = DateTime.Now.ToString("dd-M-yyyy--HH-mm-ss") + ".e4d";
            }

            using (var file = new FileStream(path + filename, FileMode.Create))
            {
                Directory.CreateDirectory(path);
                Serializer.Serialize(file, data);
            }
        }
    }

    private void SaveIbiAsCsv()
    {
        if (recordedData.Count <= 0)
        {
            return;
        }

        List<string> linesList = new List<string>();

        foreach (var data in recordedData)
        {
            var ibi = (InterbeatIntervalHeartbeat)data;

            if (data != null)
            {
                linesList.Add(string.Format("{0},{1}", ibi.timestamp, ibi.ibi));
            }
        }

        var linesArray = linesList.ToArray();

        if (UseGenericName)
        {
            filename = DateTime.Now.ToString("dd-M-yyyy--HH-mm-ss") + ".csv";
        }

        Directory.CreateDirectory(path);
        File.WriteAllLines(path + filename, linesArray);
    }

    [ContextMenu("Load and replay file")]
    public void LoadAndReplayFile()
    {
        var stream = File.OpenRead(path + filename);

        var loadedData = Serializer.Deserialize<E4Data[]>(stream);

        replayCoroutine = StartCoroutine(ReplayRoutine(loadedData));
    }

    [ContextMenu("Stop file replay")]
    public void StopFileReplay()
    {
        StopCoroutine(replayCoroutine);
    }

    IEnumerator ReplayRoutine(E4Data[] data)
    {
        var receiver = E4DataReceiver.Instance;

        while (true)
        {
            for (int i = 0; i < data.Length; i++)
            {
                E4Data d = data[i];

                if (d is Acceleration)
                {
                    var a = d as Acceleration;
                    receiver.OnAccelerationReceived.Invoke(a.x, a.y, a.z, a.timestamp);

                    //Debug.LogFormat("x:{0} y:{1} z:{2}", a.x, a.y, a.z);
                }
                else if (d is Temperature)
                {
                    var t = d as Temperature;
                    receiver.OnSkinTemperatureReceived.Invoke(t.temperature, t.timestamp);
                }
                else if (d is InterbeatIntervalHeartbeat)
                {
                    var ibi = d as InterbeatIntervalHeartbeat;
                    receiver.OnInterbeatIntervalHeartbeatReceived.Invoke(ibi.ibi, ibi.timestamp);
                }
                else if (d is GalvanicSkinResponse)
                {
                    var g = d as GalvanicSkinResponse;
                    receiver.OnGalvanicSkinResponseReceived.Invoke(g.gsr, g.timestamp);
                }
                else if (d is BloodVolumePulse)
                {
                    var b = d as BloodVolumePulse;
                    receiver.OnGalvanicSkinResponseReceived.Invoke(b.bvp, b.timestamp);
                    //Debug.Log("bvp: " + b.bvp);
                }
                else if (d is BatteryLevel)
                {
                    var b = d as BatteryLevel;
                    receiver.OnBatteryLevelReceived.Invoke(b.batteryLevel, b.timestamp);
                }

                yield return new WaitForSeconds(replaySleepTime);
            }
        }
    }

    public void AddAcceleration(int x, int y, int z, double timestamp)
    {
        recordedData.Add(new Acceleration() { timestamp = timestamp, x = x, y = y, z = z });
    }

    public void AddTemperature(float temp, double timestamp)
    {
        recordedData.Add(new Temperature() { timestamp = timestamp, temperature = temp });
    }

    public void AddIbi(float ibi, double timestamp)
    {
        recordedData.Add(new InterbeatIntervalHeartbeat() { timestamp = timestamp, ibi = ibi });
    }

    public void AddGsr(float gsr, double timestamp)
    {
        recordedData.Add(new GalvanicSkinResponse() { timestamp = timestamp, gsr = gsr });
    }

    public void AddBvp(float bvp, double timestamp)
    {
        recordedData.Add(new BloodVolumePulse() { timestamp = timestamp, bvp = bvp });
    }

    public void AddBatteryLevel(float bat, double timestamp)
    {
        recordedData.Add(new BatteryLevel() { timestamp = timestamp, batteryLevel = bat });
    }
}


[ProtoContract]
[ProtoInclude(2, typeof(Acceleration))]
[ProtoInclude(3, typeof(Temperature))]
[ProtoInclude(4, typeof(InterbeatIntervalHeartbeat))]
[ProtoInclude(5, typeof(GalvanicSkinResponse))]
[ProtoInclude(6, typeof(BloodVolumePulse))]
[ProtoInclude(7, typeof(BatteryLevel))]
public class E4Data
{
    [ProtoMember(1)]
    public double timestamp;
}

[ProtoContract]
public class Acceleration : E4Data
{
    [ProtoMember(1)]
    public int x;
    [ProtoMember(2)]
    public int y;
    [ProtoMember(3)]
    public int z;
}

[ProtoContract]
public class Temperature : E4Data
{
    [ProtoMember(1)] public float temperature;
}

[ProtoContract]
public class InterbeatIntervalHeartbeat : E4Data
{
    [ProtoMember(1)] public float ibi;
}

[ProtoContract]
public class GalvanicSkinResponse : E4Data
{
    [ProtoMember(1)] public float gsr;
}

[ProtoContract]
public class BloodVolumePulse : E4Data
{
    [ProtoMember(1)] public float bvp;
}

[ProtoContract]
public class BatteryLevel : E4Data
{
    [ProtoMember(1)] public float batteryLevel;
}